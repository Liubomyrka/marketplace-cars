from os import system
from settings import *
from random import *
from pickle import load, dump


class Car:
    def __init__(self, brand: str = "", model: str = "", year: int = 0, mileage: float = 0, color: str = "",
                 type_car: str = "", fuel: str = "", type_of_drive: str = "", volume: float = 0, power: int = 0,
                 condition: str = "", transmission: str = "", description: str = "", bargain: bool = False,
                 price: float = 0, usd: bool = False, name: str = "", telephone: int = 0, email: str = "",
                 city: str = ""):
        if brand == "r" or brand == "t" or brand == "test":
            self.random_car()
        else:
            self._brand = brand  # Марка
            self._model = model  # Модель
            self._year = year  # Рік виготовлення
            self._mileage = mileage  # Пробіг
            self._color = color  # Колір
            self._type_car = type_car  # Тип авто
            self._fuel = fuel  # Тип палива
            self.type_of_drive = type_of_drive  # Тип приводу
            self._volume = volume  # Об'єм
            self._power = power  # Кінські сили
            self._condition = condition  # Стан авто
            self._transmission = transmission  # Тип коробки передач
            self._description = description  # Опис
            self._bargain = bargain  # Торг
            self._price = price  # Ціна
            self._usd = usd  # У $?
            self._name = name  # Ім'я
            self._telephone = telephone  # Телефон
            self._email = email  # Пошта
            self._city = city  # Місто

    def __str__(self):
        return f"Марка: {self._brand}\n" \
               f"Модель: {self._model}\n" \
               f"Рік виготовлення: {self._year}\n" \
               f"Пробіг: {self._mileage} км.\n" \
               f"Колір: {self._color}\n" \
               f"Тип авто: {self._type_car}\n" \
               f"Тип палива: {self._fuel}\n" \
               f"Об'єм: {self._volume}\n" \
               f"Кінські сили: {self._power} к.с.\n" \
               f"Тип приводу: {self.type_of_drive}\n" \
               f"Стан авто: {self._condition}\n" \
               f"Тип коробки передач: {self._transmission}\n" \
               f"----------------------------------------------------\n" \
               f"Опис: {self._description}\n" \
               f"----------------------------------------------------\n" \
               f"Ціна: {self.get_price_full()}\n" \
               f"Ім'я: {self._name}\n" \
               f"Телефон: {self.get_telephone()}\n" \
               f"Пошта: {self._email}\n" \
               f"Місто: {self._city}"

    def __call__(self, *args):
        return str(f"{self._brand} - {self._model} - {self._color} > {self._mileage} км. - {self.get_price()}")

    def get_brand(self):
        return self._brand

    def get_telephone(self):
        telephone = str(self._telephone)
        return f"{telephone[:2]} {telephone[2:5]}-{telephone[5:8]}-{telephone[8:]}"

    def get_price(self):
        price = str(self._price)
        if self._usd:
            price += f"$"
        else:
            price += f" грн."
        if self._bargain:
            price += " - Договірна"
        return price

    def get_price_full(self):
        price = str(self._price)
        if self._usd:
            price += f"$ - {int(self._price * USD)} грн."
        else:
            price += f" грн. - {int(self._price / USD)}$"
        if self._bargain:
            price += " - Договірна"
        return price

    # ///////////////////////////// RANDOM /////////////////////////////
    def random_car(self):
        self.set_brand_r()
        self.set_model_r()
        self.set_volume_r()
        self.set_usd_r()
        self.set_year_r()
        self.set_type_of_drive_r()
        self.set_transmission_r()
        self.set_type_car_r()
        self.set_telephone_r()
        self.set_price_r()
        self.set_power_r()
        self.set_name_r()
        self.set_mileage_r()
        self.set_fuel_r()
        self.set_email_r()
        self.set_description_r()
        self.set_condition_r()
        self.set_color_r()
        self.set_city_r()
        self.set_bargain_r()

    def set_brand_r(self):
        self._brand = choice(BRANDS)

    def set_model_r(self):
        self._model = MODEL

    def set_year_r(self):
        self._year = randint(1900, 2022)

    def set_mileage_r(self):
        self._mileage = randint(10, 1000000)

    def set_color_r(self):
        self._color = choice(COLORS)

    def set_type_car_r(self):
        self._type_car = choice(TYPE_CAR)

    def set_fuel_r(self):
        self._fuel = choice(FUELS)

    def set_volume_r(self):
        self._volume = round(uniform(0.5, 6.0), 2)

    def set_power_r(self):
        self._power = randint(50, 600)

    def set_type_of_drive_r(self):
        self.type_of_drive = choice(TYPE_OF_DRIVE)

    def set_condition_r(self):
        self._condition = choice(CONDITIONS)

    def set_transmission_r(self):
        self._transmission = choice(TRANSMISSIONS)

    def set_description_r(self):
        self._description = DESCRIPTION

    def set_bargain_r(self):
        self._bargain = bool(randint(0, 1))

    def set_price_r(self):
        self._price = round(uniform(850.0, 55000.0), 2)

    def set_usd_r(self):
        self._usd = bool(randint(0, 1))

    def set_name_r(self):
        self._name = NAME

    def set_telephone_r(self):
        self._telephone = randint(380000000000, 380999999999)

    def set_email_r(self):
        self._email = EMAIL

    def set_city_r(self):
        self._city = choice(CITY)


class Marketplace:
    def __init__(self):
        self.list_car: list = []

    def __call__(self, *args, **kwargs):
        self.menu()

    def menu(self):
        try:
            while True:
                system('cls')
                input_var = input("Menu:\n"
                                  "0. Швидкий перегляд\n"
                                  "1. Детальний Перегляд\n"
                                  "2. Додавання\n"
                                  "3. Видалення\n"
                                  "4. Пошук\n"
                                  "5. Файл\n"
                                  "6. Вихід\n"
                                  " >>> ")

                if input_var == "1":

                    self.print()
                    input()

                elif input_var == "0":

                    num_car = 0
                    for car in self.list_car:
                        print(f"{num_car+1}. {car()}")
                        num_car += 1
                    input()

                elif input_var == "2":

                    input_var = input("1. Додати власноруч\n"
                                      "2. Додати random\n"
                                      " >>> ")
                    if input_var == "1":

                        brand = input("Марка: ")  # Марка
                        model = input("Модель: ")  # Модель
                        year = int(input("Рік виготовлення: "))  # Рік виготовлення
                        mileage = float(input("Пробіг: "))  # Пробіг
                        color = input("Колір: ")  # Колір
                        type_car = input("Тип авто: ")  # Тип авто
                        fuel = input("Тип палива: ")  # Тип палива
                        type_of_drive = input("Тип приводу: ")  # Тип приводу
                        volume = float(input("Об'єм: "))  # Об'єм
                        power = int(input("Кінські сили: "))  # Кінські сили
                        condition = input("Стан авто: ")  # Стан авто
                        transmission = input("Тип коробки передач: ")  # Тип коробки передач
                        description = input("Опис: ")  # Опис
                        bargain = bool(input("Торг: "))  # Торг
                        price = int(input("Ціна: "))  # Ціна
                        usd = bool(input("У $?: "))  # У $?
                        name = input("Ім'я: ")  # Ім'я
                        telephone = int(input("Телефон: "))  # Телефон
                        email = input("Пошта: ")  # Пошта
                        city = input("Місто: ")  # Місто
                        self.add(brand, model, year, mileage, color, type_car, fuel,
                                 type_of_drive, volume, power, condition, transmission,
                                 description, bargain, price, usd, name, telephone, email, city)

                    elif input_var == "2":

                        try:
                            self.add_r(int(input("Скільки додати? >>> ")))
                        except ValueError:
                            self.add_r()

                elif input_var == "3":

                    num_car = 0
                    for car in self.list_car:
                        print(f"{num_car+1}. {car()}")
                        num_car += 1
                    try:
                        input_var = int(input(" >>> "))
                        info = f"{input_var}. {self.list_car[input_var-1]()}"
                        self.list_car.pop(input_var-1)
                        input(f"Видалено! -> {info}")
                    except ValueError:
                        input("Не правильний ввод!")

                elif input_var == "4":

                    input_var = input("Введіть марку авто: ")
                    print("\n")
                    for car in self.list_car:
                        # print(str(car.get_brand()).lower(), input_var.lower())
                        if str(car.get_brand()).lower() == input_var.lower():
                            print(car, "\n\n####################################################\n")
                    input()

                elif input_var == "5":

                    if input("1. Читати\n2. Записати\n >>> ") == "1":
                        if input("1. Перезаписати\n2. Додати\n >>> ") == "1":
                            with open(SAVE_FILE, 'rb') as file:
                                self.list_car = load(file)
                            input(f"Файл зчитано! {len(self.list_car)} машин імпортовано")
                        else:
                            with open(SAVE_FILE, 'rb') as file:
                                list_var = load(file)
                                self.list_car += list_var
                            input(f"Файл зчитано! {len(list_var)} машин додано")
                    else:
                        with open(SAVE_FILE, 'wb') as file:
                            dump(self.list_car, file)
                        input(f"Файл записано! {len(self.list_car)} машин експортовано")

                elif input_var == "6":

                    print("Завершення программи!")
                    exit()

                else:
                    input("Такого пункту не знайдено, спробуйте ще")

        except KeyboardInterrupt:
            print("Завершення программи!")
            exit()

    def add_r(self, r: int = 1):
        for num in range(r):
            self.list_car.append(Car("r"))

    def add(self, brand: str, model: str, year: int, mileage: float, color: str,
            type_car: str, fuel: str, type_of_drive: str, volume: float, power: int,
            condition: str, transmission: str, description: str, bargain: bool,
            price: float, usd: bool, name: str, telephone: int, email: str,
            city: str):
        self.list_car.append(Car(brand, model, year, mileage, color, type_car, fuel,
                                 type_of_drive, volume, power, condition, transmission,
                                 description, bargain, price, usd, name, telephone, email, city))

    def __str__(self):
        list_car = ""
        for car in self.list_car:
            list_car += str(car) + "\n\n####################################################\n\n"
        return list_car

    def print(self):
        print(self)


if __name__ == '__main__':
    market = Marketplace()
    market.add_r(START)
    print(market)
    market()
